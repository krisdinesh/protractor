// tslint:disable-next-line: prefer-const
let { SpecReporter } = require('jasmine-spec-reporter');

import { Config } from 'protractor';

export let config: Config = {

  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: ["--headless", "--disable-gpu", "--window-size=800x600", "--no-sandbox"]
      // args: ['--disable-gpu', '--window-size=800x600', '--no-sandbox']
    }

  },
  directConnect: true,
  baseUrl: 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print() { }
  },
  specs: ['./Spec/IpcLoginSpec.js'],
  // seleniumAddress: 'http://localhost:4444/wd/hub',
  // You could set no globals to true to avoid jQuery '$' and protractor '$'
  // collisions on the global namespace.
  onPrepare() {
    // console logs configurations
    jasmine.getEnv().addReporter(new SpecReporter({
      displayStacktrace: 'all',      // display stacktrace for each failed assertion, values: (all|specs|summary|none) 
      displaySuccessesSummary: false, // display summary of all successes after execution 
      displayFailuresSummary: true,   // display summary of all failures after execution 
      displayPendingSummary: true,    // display summary of all pending specs after execution 
      displaySuccessfulSpec: true,    // display each successful spec 
      displayFailedSpec: true,        // display each failed spec 
      displayPendingSpec: false,      // display each pending spec 
      displaySpecDuration: false,     // display each spec duration 
      displaySuiteNumber: false,      // display each suite number (hierarchical) 
      colors: {
        success: 'green',
        failure: 'red',
        pending: 'yellow'
      },
      prefixes: {
        success: '✓ ',
        failure: '✗ ',
        pending: '* '
      },
      customProcessors: []
    }));

  },
  noGlobals: true
};
