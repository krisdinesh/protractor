import { browser, element, by, protractor } from 'protractor';
import fetch from 'cross-fetch';



export enum IdentificationType {
        Id,
        Name,
        Css,
        Xpath,
        PartialLinkText,
        ClassName,
        Js

}

export class BasePage {

        ElementLocator(obj) {

                switch (obj.type) {
                        case IdentificationType[IdentificationType.Id]:
                                return element(by.Id(obj.value));
                        case IdentificationType[IdentificationType.Name]:
                                return element(by.Name(obj.value));
                        case IdentificationType[IdentificationType.Css]:
                                return element(by.css(obj.value));
                        case IdentificationType[IdentificationType.Xpath]:
                                return element(by.xpath(obj.value));
                        case IdentificationType[IdentificationType.PartialLinkText]:
                                return element(by.PartialLinkText(obj.value));
                        case IdentificationType[IdentificationType.ClassName]:
                                return element(by.ClassName(obj.value));
                        case IdentificationType[IdentificationType.Js]:
                                return element(by.js(obj.value));
                }
        }

        RandomString(x: number): string {
                let outString = '';
                const inOptions = 'abcdefghijklmnopqrstuvwxyz';
                const str = 'test';

                for (let i = 0; i < x; i++) {

                        outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));

                }
                outString = str.concat(outString);
                return outString;
        }

}
