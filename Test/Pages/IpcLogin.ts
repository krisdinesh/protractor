import { browser, protractor } from 'protractor';
import { IdentificationType, BasePage } from './BasePage';
import { Driver } from 'selenium-webdriver/chrome';
import { timeout, delay } from 'q';
import { del } from 'selenium-webdriver/http';
import { DirectiveResolver } from '@angular/compiler';

// Getting the elements from the Login page

const Locators = {
    userName: {
        type: IdentificationType[IdentificationType.Xpath],
        value: '//input[@id="normal_login_email"]'
    },
    password: {
        type: IdentificationType[IdentificationType.Xpath],
        value: '//input[@id="normal_login_password"]'
    },
    btnLogin: {
        type: IdentificationType[IdentificationType.Xpath],
        value: '//button[@class="login-form-button ant-btn ant-btn-primary"]'
    },
    linkForgotPassword: {
        type: IdentificationType[IdentificationType.Xpath],
        value: '//a[@class="login-form-forgot"]'
    },
    invalidUserPass: {
        type: IdentificationType[IdentificationType.Xpath],
        value: '//span[@class="ant-alert-message"]'
    },

};

export class IpcLogin extends BasePage {

    username = this.ElementLocator(Locators.userName);
    password = this.ElementLocator(Locators.password);
    btnLogin = this.ElementLocator(Locators.btnLogin);
    invaliduserpass = this.ElementLocator(Locators.invalidUserPass);

    // open browser
    OpenBrowser(url: string) {

        // set browserto full screen
        browser.manage().window().maximize();

        // for non-angular page
        browser.waitForAngularEnabled(false);

        // Navigate to the provided URL
        browser.get(url);

    }

    async Login(uname: string, pwd: string) {
        await this.username.sendKeys(uname);
        await this.password.sendKeys(pwd);
        await this.btnLogin.click();
    }

    async ClearLoginPageFields() {
        await this.username.clear();
        await this.password.clear();
    }
}
