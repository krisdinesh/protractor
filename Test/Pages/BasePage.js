"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
var IdentificationType;
(function (IdentificationType) {
    IdentificationType[IdentificationType["Id"] = 0] = "Id";
    IdentificationType[IdentificationType["Name"] = 1] = "Name";
    IdentificationType[IdentificationType["Css"] = 2] = "Css";
    IdentificationType[IdentificationType["Xpath"] = 3] = "Xpath";
    IdentificationType[IdentificationType["PartialLinkText"] = 4] = "PartialLinkText";
    IdentificationType[IdentificationType["ClassName"] = 5] = "ClassName";
    IdentificationType[IdentificationType["Js"] = 6] = "Js";
})(IdentificationType = exports.IdentificationType || (exports.IdentificationType = {}));
class BasePage {
    ElementLocator(obj) {
        switch (obj.type) {
            case IdentificationType[IdentificationType.Id]:
                return protractor_1.element(protractor_1.by.Id(obj.value));
            case IdentificationType[IdentificationType.Name]:
                return protractor_1.element(protractor_1.by.Name(obj.value));
            case IdentificationType[IdentificationType.Css]:
                return protractor_1.element(protractor_1.by.css(obj.value));
            case IdentificationType[IdentificationType.Xpath]:
                return protractor_1.element(protractor_1.by.xpath(obj.value));
            case IdentificationType[IdentificationType.PartialLinkText]:
                return protractor_1.element(protractor_1.by.PartialLinkText(obj.value));
            case IdentificationType[IdentificationType.ClassName]:
                return protractor_1.element(protractor_1.by.ClassName(obj.value));
            case IdentificationType[IdentificationType.Js]:
                return protractor_1.element(protractor_1.by.js(obj.value));
        }
    }
    RandomString(x) {
        let outString = '';
        const inOptions = 'abcdefghijklmnopqrstuvwxyz';
        const str = 'test';
        for (let i = 0; i < x; i++) {
            outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));
        }
        outString = str.concat(outString);
        return outString;
    }
}
exports.BasePage = BasePage;
//# sourceMappingURL=BasePage.js.map