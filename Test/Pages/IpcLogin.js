"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
const BasePage_1 = require("./BasePage");
// Getting the elements from the Login page
const Locators = {
    userName: {
        type: BasePage_1.IdentificationType[BasePage_1.IdentificationType.Xpath],
        value: '//input[@id="normal_login_email"]'
    },
    password: {
        type: BasePage_1.IdentificationType[BasePage_1.IdentificationType.Xpath],
        value: '//input[@id="normal_login_password"]'
    },
    btnLogin: {
        type: BasePage_1.IdentificationType[BasePage_1.IdentificationType.Xpath],
        value: '//button[@class="login-form-button ant-btn ant-btn-primary"]'
    },
    linkForgotPassword: {
        type: BasePage_1.IdentificationType[BasePage_1.IdentificationType.Xpath],
        value: '//a[@class="login-form-forgot"]'
    },
    invalidUserPass: {
        type: BasePage_1.IdentificationType[BasePage_1.IdentificationType.Xpath],
        value: '//span[@class="ant-alert-message"]'
    },
};
class IpcLogin extends BasePage_1.BasePage {
    constructor() {
        super(...arguments);
        this.username = this.ElementLocator(Locators.userName);
        this.password = this.ElementLocator(Locators.password);
        this.btnLogin = this.ElementLocator(Locators.btnLogin);
        this.invaliduserpass = this.ElementLocator(Locators.invalidUserPass);
    }
    // open browser
    OpenBrowser(url) {
        // set browserto full screen
        protractor_1.browser.manage().window().maximize();
        // for non-angular page
        protractor_1.browser.waitForAngularEnabled(false);
        // Navigate to the provided URL
        protractor_1.browser.get(url);
    }
    Login(uname, pwd) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.username.sendKeys(uname);
            yield this.password.sendKeys(pwd);
            yield this.btnLogin.click();
        });
    }
    ClearLoginPageFields() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.username.clear();
            yield this.password.clear();
        });
    }
}
exports.IpcLogin = IpcLogin;
//# sourceMappingURL=IpcLogin.js.map