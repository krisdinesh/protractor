"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
const IpcLogin_1 = require("../Pages/IpcLogin");
describe('Login Verification', () => {
    const ipcLogin = new IpcLogin_1.IpcLogin();
    it('Should open the browser and navigate to IPC Timesheet Login page', () => {
        // Open browser
        ipcLogin.OpenBrowser('http://34.205.183.151');
    });
    it('Should be able to login with valid user credentials', () => __awaiter(this, void 0, void 0, function* () {
        yield ipcLogin.Login('ramusarithak@gmail.com', 'R@mu1234');
        yield protractor_1.browser.sleep(5000);
    }));
});
//# sourceMappingURL=IpcLoginSpec.js.map