import { browser, element, by } from 'protractor';
import { IpcLogin } from '../Pages/IpcLogin';
import { BasePage } from '../Pages/BasePage';
import { async, timeout } from 'q';

describe('Login Verification', () => {

    const ipcLogin = new IpcLogin();

    it('Should open the browser and navigate to IPC Timesheet Login page', () => {

        // Open browser
        ipcLogin.OpenBrowser('http://34.205.183.151');

    });

    it('Should be able to login with valid user credentials', async () => {

        await ipcLogin.Login('ramusarithak@gmail.com', 'R@mu1234');
        await browser.sleep(5000);
    });

});




