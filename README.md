1. Install VS code
	- open VS Code naviage to project folder and open terminal
2. Install node js
3. Install protractor - npm install protractor -g
4. Install Jasmins - npm install jasmine
5. Install types for Jasmine - npm install --save-dev @types/jasmine
6. Install Typescript - npm install typescript
7. Update WEBDRIVER - webdriver-manager update
8. Chrome browser version 77



Setup new Project:
==================

1. Install Visual Studio Code
2. Install Node
3. npm install -g @angular/cli
4. Open the command prompt and navigate to the folder where you want to create Project
5. ng new HomeLoans 
6. Open VS code from the folder HomeLoans
7. Open Terminal from vs code and run these commands
	- ng serve
	- Check you are able to access the browser in this URL - http://localhost:4200/webpack-dev-server/
	- npm install --save-dev jasmine
	- npm install --save @types/jasmine
	- npm install @types/node@8   //to fix the issue 'tsc -w' compiler issue
	- npm install -g protractor
	- npm install --save-dev protractor
	- webdriver-manager install
	- npm install jasminewd2
	- npm install -g typescript
	- npm update --save-dev typescript
8. Create a Folder 'Test' underneath 'HomeLoans'
	- Create FirstTestSpec.ts file
	- Add SpecRunner.html
	- Add Conf.ts
	- Add tsconfig.json
9. under the folder add launch.json file
10 Make sure you have firefox version not greater than 57 if you want to run tests in firefox browser


Important things to remember:
=============================

	- 'tsc -w' is the command for auto compiling the code in incremental manner whenever any change to the files
		- Note: When new .ts file is created and saved, you will be seeing the two other files created with same name with the extension .js and .js.map respectively when tsc-w command is active
	- To run the Test open cmd as admin run 'webdriver-manager start' and keep the session alive
		- Open another cmd as admin
		- Navigae to the Test folder
		- Run the command 'protractor conf.js'
			- Always we will use or refer .js files during the run or any configuration which were auto-created by Typescript compiler(tsc). 
			- Typescript(.ts) files contains the automation code written by us where Typescript compiler will create the .js and .js.map. 
	- Currently we are using chrome version 77. Otherwise test will not run due to driver mismatch
	- browser.ignoreSynchronization = true is set because login page is non-angular. Please note this is depcrecated by protractor and soon will not be available to use in the future upgrade to protractor version. 
	- When you see this message in problem tab "No inputs were found in config". Please restart your VScode which will resolve this issue and it is intermittent. 
  - While starting web driver you get driver version unknown, run these commads 'webdriver-manager clean' and 'webdriver-manager update' to clear the binaries and download them again.

As soon as the code is cloned from repo, run the command 'npm install' from project root folder

git commands:
==============

git clone <repourl>

git checkout -b <branchname> --creates branch in local

git add . -- track files

git status --to check files are tracked

git commit -m <comment> --commit into local machin

git push --set-upstream origin <branchname> --push the branch to repo










